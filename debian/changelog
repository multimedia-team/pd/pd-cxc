pd-cxc (0.5.2-8) unstable; urgency=medium

  * Patch to use pd_error() instead of error() (Closes: #1066627)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 16 Mar 2024 21:44:54 +0100

pd-cxc (0.5.2-7) unstable; urgency=medium

  * Re-build for both single and double-precision Pd
  * Switch build-system to pd-lib-builder
    + Replace upstream build-system with pd-lib-builder
  * Add lintian-overrides for false-positives regarding underlinking
  * Bump standards version to 4.6.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 05 Jul 2023 14:01:30 +0200

pd-cxc (0.5.2-6) unstable; urgency=medium

  * Modernize 'licensecheck' target
    + Ensure that 'licensecheck' is run with the C.UTF-8 locale
    + Exclude debian/ from 'licensecheck'
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 07 Dec 2022 10:38:37 +0100

pd-cxc (0.5.2-5) unstable; urgency=medium

  * Modernize 'licensecheck' target
    + Re-generated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 24 Nov 2022 23:26:52 +0100

pd-cxc (0.5.2-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Archive, Bug-Database.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 28 Sep 2022 18:09:28 +0200

pd-cxc (0.5.2-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Make sure that CPPFLAGS are applied
  * Removed Hans-Christoph Steiner from Uploaders as he requested
  * Add salsa-ci configuration
  * Remove obsolete file d/source/local-options
  * Declare that building this package does not require 'root' powers.
  * Apply "warp-and-sort -ast"
  * Bump dh-compat to 13
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 04 Sep 2022 13:27:15 +0200

pd-cxc (0.5.2-2) unstable; urgency=medium

  * Patch for arrays on 64bit architectures (Closes: #792719)
  * Patch to disable almost all compiler warnings
  * Enabled hardening
  * Bumped dh compat to 11
    * Simplified & unified d/rules
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Switched URLs to https://
  * Added myself to Uploaders
  * Updated d/copyright(_hints)
  * Removed trailing whitespace in debian/*
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:00:31 +0100

pd-cxc (0.5.2-1) unstable; urgency=low

  * Depends: puredata-core | pd, so the depends is not only on a virtual package
  * updated to copyright-format/1.0
  * Standards-Version: 3.9.4
  * Imported Upstream version 0.5.2
  * remove non-linux fixes patches since they are now included upstream

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 17 Jan 2013 23:15:41 -0500

pd-cxc (0.5.1-2) unstable; urgency=low

  [ Alessio Treglia ]
  * Add Vcs-* tags.

  [ IOhannes m zmölnig ]
  * Makefile-fixes for non-linux (Closes: #609436)

  [ Hans-Christoph Steiner ]
  * updated Build-Depends to use puredata-dev when available
        (Closes: #629704)
  * bumped standards version to 3.9.2

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 10 Jun 2011 12:33:41 -0400

pd-cxc (0.5.1-1) unstable; urgency=low

  * Initial release (Closes: #591847)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 11 Nov 2010 10:53:01 -0500
